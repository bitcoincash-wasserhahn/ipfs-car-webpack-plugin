const webpack = require("webpack");
const path = require("path");

const IPFS = require('ipfs');
const glob = require('glob');
const all = require('it-all');
const last = require("it-last");

const { MemoryDatastore } = require('datastore-core');
const { MemoryBlockstore } = require('blockstore-core');

const { createRepo } = require('ipfs-repo');
const { MemoryLock } = require('ipfs-repo/locks/memory');
const rawCodec = require('multiformats/codecs/raw');
const EventEmitter = require("events");

const repo = createRepo(
	'',
	async () => rawCodec,
	{
		blocks: new MemoryBlockstore(),
		datastore: new MemoryDatastore(),
		keys: new MemoryDatastore(),
		pins: new MemoryDatastore(),
		root: new MemoryDatastore()
	},
	{ autoMigrate: false, repoLock: MemoryLock, repoOwner: true }
);

class FakeTransport {
  [Symbol.toStringTag]() {
    return "FakeTransport"
  }

  filter(addrs) {
    return addrs
  }

  createListener() {
    const listener = new EventEmitter();
    listener.listen = function() {

    }
    listener.getAddrs = function() {
      return []
    }
    return listener
  }
}

module.exports = class CarAssetPlugin {
  constructor(options) {
    if (!options.path) throw new Error("'path' option required")
    if (!options.filename) throw new Error("'filename' option required")
    
    options.pattern = options.pattern || "**/*";

    this.options = options;
  }

  /**
   * @param {webpack.Compiler} compiler 
   */
  apply(compiler) {
    compiler.hooks.thisCompilation.tap("CarAssetPlugin", (compilation) => {
      compilation.hooks.processAssets.tapAsync({
        name: "CarAssetPlugin",
        stage: compiler.webpack.Compilation.PROCESS_ASSETS_STAGE_ADDITIONAL,
      }, async (assets, callback) => {

        /** @type {IPFS.IPFS} */
        const ipfs = await IPFS.create({ 
          repo, 
          offline: true, 
          silent: true, 
          libp2p: {
            modules: {
              transport: [FakeTransport]
            }
          } 
        });
        
        const files = await Promise.all(glob.sync(this.options.pattern || "*", {
          cwd: this.options.path,
          nodir: true,
          fs: compilation.inputFileSystem
        }).map(async (relativePath) => {
          const absPath = path.resolve(this.options.path, relativePath);
          
          compilation.fileDependencies.add(absPath);

          return {
            path: relativePath,
            content: await new Promise((resolve, reject) => compilation.inputFileSystem.readFile(absPath, (err, content) => {
              if (err) return reject(err);
              return resolve(content);
            }))
          }
        }));

        const result = await last(ipfs.addAll(files, { wrapWithDirectory: true }));
        const carFile = Buffer.concat(await all(ipfs.dag.export(result.cid)));

        compilation.emitAsset(this.options.filename, new compiler.webpack.sources.RawSource(carFile));

        try { await ipfs.stop(); } catch (err) { }

        callback(null)
      })
    });
  }
}